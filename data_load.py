import requests
import numpy as np
import time
import db_doer
import matplotlib

BASE = {
    "URL": "https://dt.miet.ru/ppe_it/api",
    "DELAY": 3,
    "DURATION": 210,
    "dbname": "./database_1.db",
    "logger": {
        "is_enabled": True
    },
    # переход city_id -> city_name и city_name -> city_id
    "cities": {},
    "token": "gzx29l495x9tfpkr"
      }

def logger_on():
    BASE["logger"]["is_enabled"] = True

def log_off():
    BASE["logger"]["is_enabled"] = False
    
def log(message, level='INFO'):
    #сообщения
    if BASE.get('logger', {}).get('is_enabled', False):
        print(f"[{level}]:\n\t{message}\n")
        
def initialize(cities):
    """Функция инициализации"""
    db_doer.initialize(
      cities = load_cities_temperature(cities),
      connect = db_doer.open_connection(BASE['dbname'])
    )

    for city in cities:
        BASE["cities"][city["city_id"]] = city["city_name"]
        BASE["cities"][city["city_name"]] = city["city_id"]

def load_data(url):
    """Функция загружает данные."""
    result = {"data": None, "error": None}
    try:
        log(f">>> REQ: {url} ")
        res = requests.get(url, headers={'X-Auth-Token': BASE.get('token')})
        msg = f"<<< RES: {res.status_code}\n\t{res.text}"
        if res.status_code == 200:
            result["data"] = res.json()
            log(msg)
        else:
            result["error"] = res.text
            log(msg, "ERROR")
    except Exception as error:
        log(str(error), "ERROR")
        result["error"] = str(error)
    return result

def get_cities():
    """Функция возвращает список городов."""
    res = load_data(BASE["URL"])
    return res["data"].get("data") or []


def load_cities_temperature(cities=None):
    """Загрузка данных о температуре в городах."""
    if cities is None:
        cities = get_cities()
    for city in cities:
        # city: {"city_id":5,"city_name":"Курортный"}
        res = load_data(f"{BASE['URL']}/{city['city_id']}")
        if res["data"] is not None:
            city.update(res["data"]["data"])
            city.update({"day": res["data"]["day"]})
    return cities

def now():
    ''' Возвращает текущее время '''
    return int(time.time())


def get_day():
    result = load_data(BASE["URL"])
    if result["data"] is not None:
        result["data"]["day"]

def main():
    """Главная функция."""
    #gr_times = []
    cities = get_cities()
    
    initialize(cities)
    
    beginning = now()

    while True:
        # сохранение данных температуры
        db_doer.save_cities_data(
            # загрузка данных температуры в городах
            cities=load_cities_temperature(cities),
            connect = db_doer.get_connection()
        )
        if now() - beginning >= BASE["DURATION"]:
            break
        log('wait...')
        time.sleep(BASE["DELAY"])

    db_doer.close_connection()
    print("DONE!!!")
    
'''def graphic():
    # Независимая (x) и зависимая (y) переменные
    y = [
    # Построение графика
    plt.title("Город - Научный") # заголовок
    plt.xlabel("days") # ось абсцисс
    plt.ylabel("T") # ось ординат
    plt.grid()      # включение отображение сетки
    plt.plot(x, y)  # построение графика
'''

if __name__ == "__main__":
    main()