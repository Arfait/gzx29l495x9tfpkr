import sqlite3
context = {
    "connect": None
}

def creating_table(connect):
    if connect is None:
        connect = context["connect"]
        SQL = """
        DROP TABLE IF EXISTS city;
        CREATE TABLE IF NOT EXISTS city(
            city_id INT,
            city_name TXT);
        DROP TABLE IF EXISTS city_temperature;
        CREATE TABLE IF NOT EXISTS city_temperature(
            city_id INT,
            city_name TEXT,
            temperature INT,
            day INT);
    """
    connect.executescript(SQL)
    
def open_connection(db_name):
    """Функция открывает соединения.БД."""
    if context["connect"] is None:
        context["connect"] = sqlite3.connect(database_1.db)
    return context["connect"]


def close_connection():
    """Функция закрывает соединения с БД."""
    if context["connect"] is not None:
        context["connect"].close()
        context["connect"] = None

def get_connection():
    return context["connect"]

def init(*, cities, connect = None):
    if connect is None:
        connect = context["connect"]
    creating_table(connect):
    for city in cities:
        SQL = f"""INSERT INTO city(
        city_id,
        city_name)
        VALUES(
        {city["city_id"]},
        {city["city_name"]});
        """
    connet.executescript(SQL)
    
# {"city_name": {"day": [], "temperature": []}}
def read_cities_data(connect = None):
    if connect is None:
        connect = context["connect"]
    data = {}
    SQL = "SELECT * FROM city_temeperature;"
    for row in connect.execute(SQL):
        if data.get(row[1]) is None:
            data[row[1]] = {"day": [], "temperature": []}
        data[row[1]]["day"].append(row[3])
        data[row[1]]["temperature"].append(row[2])
    return data
    
def save_cities_data(*, cities, connect = None):
    if connect is None:
        connect = context["connect"]
        
    for city in cities:
        sql = f"""
            SELECT * FROM city_temperature WHERE
                city_id = {city['city_id']} AND
                day = {city['day']}
        """
        if not list(connect.execute(sql)):
            sql = f"""
                INSERT INTO city_temperature VALUES(
                    {city['city_id']},
                    '{city['city_name']}',
                    {city['temperature']},
                    {city['day']}
                );
            """
            connect.executescript(sql)